import { DateTime } from "luxon";
import { datesAreEqual, isNewDay } from "../utils";

describe("datesAreEqual", () => {
  it("Returns true when the dates are the same", () => {
    const result = datesAreEqual(new Date(), new Date());
    expect(result).toBe(true);
  });

  it("Returns false when the dates are different", () => {
    const result = datesAreEqual(new Date(), new Date().getDate() + 1);
    expect(result).toBe(false);
  });
});

describe("isNewDay", () => {
  it("Returns true when called with today and tomorrow", () => {
    const result = isNewDay(DateTime.now(), DateTime.now().plus({ days: 1 }));
    expect(result).toBe(true);
  });

  it("Returns false when called date1 is after date2", () => {
    const result = isNewDay(DateTime.now().plus({ days: 1 }), DateTime.now());
    expect(result).toBe(false);
  });
});
