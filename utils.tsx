import { ToastAndroid, Platform } from "react-native";
import { DateTime } from "luxon";
import { HabitMap } from "./types";

/* Prints the contents of a map */
export const printMap = (map: HabitMap) => {
  console.log(Object.fromEntries([...map]));
};

/* Simple wrapper for android toast
 * Shows a short @message
 * TODO: Make platform independent */
export const showAndroidToast = (message: string) => {
  Platform.OS === "android"
    ? ToastAndroid.show(message, ToastAndroid.SHORT)
    : console.log("Sorry, I haven't implemented plaform independent alerts");
};

/* Returns true if two Date objects are the same */
export const datesAreEqual = (date1: Date, date2: Date): boolean => {
  return (
    date1.getFullYear == date2.getFullYear &&
    date1.getMonth == date2.getMonth &&
    date1.getDay == date2.getDay
  );
};

/* Returns true if date2 is later than date1 */
export const isNewDay = (date1: DateTime, date2: DateTime): boolean => {
  return date2.startOf("day") > date1.startOf("day");
};
