import { StyleSheet } from "react-native";
import { colors } from "./colors";

export const globalStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    alignSelf: "center",
    padding: 10,
    borderRadius: 10,
    backgroundColor: colors.brandColor,
    marginBottom: 10,
    marginTop: 10,
  },
  success: {
    backgroundColor: colors.success,
  },
  danger: {
    backgroundColor: colors.danger,
  }
});
