// export const palette = {
//   "Red Salsa": "#f94144",
//   "Orange Red": "#f3722c",
//   "Yellow Orange Color Wheel": "#f8961e",
//   "Alice Blue": "#e7ebef",
//   // compat
//   Nickel: "#747572",
//   "Deep Saffron": "#f9a03f",
//   "Dark Jungle Green": "#041b15",
//   "Yellow Green Crayola": "#bbd686",
//   "Sky Blue": "#84cae7",
//   "Rich Black FOGRA 29": "#111518",
// };

// export const abstractColors = {
//   red: palette["Red Salsa"],
//   orange: palette["Orange Red"],
//   yellow: palette["Yellow Orange Color Wheel"],
//   blue: palette["Sky Blue"],
//   gold: palette["Deep Saffron"],
//   green: palette["Yellow Green Crayola"],
//   white: palette["Alice Blue"],
//   black: palette["Rich Black FOGRA 29"],
//   gray: palette["Nickel"],
// };

export const palette = {
  "Ghost White": "#f3f3f7",
  "Rich Black FOGRA 39": "#0b0a07",
  Olivine: "#99bc76",
  "Hunter Green": "#4b644a",
  "Carolina Blue": "#4ea5d9",
  "Blue Munsell": "#508ca4",
  "Bright Yellow Crayola": "#f5aa29",
  "Fire Opal": "#ee6055",
  Silver: "#CACACE",
  Manatee: "#95959D",
};

export const abstractColors = {
  red: palette["Fire Opal"],
  yellow: palette["Bright Yellow Crayola"],
  blue: palette["Carolina Blue"],
  blue2: palette["Blue Munsell"],
  green: palette["Olivine"],
  green2: palette["Hunter Green"],
  white: palette["Ghost White"],
  black: palette["Rich Black FOGRA 39"],
  lgray: palette["Silver"],
  dgray: palette["Manatee"],
};

export const colors = {
  brandColor: abstractColors.green,
  primaryText: abstractColors.black,
  secondaryText: abstractColors.dgray,
  bgLight: abstractColors.white,
  bgdGray: abstractColors.dgray,
  bglGray: abstractColors.lgray,
  danger: abstractColors.red,
  success: abstractColors.green,
};
