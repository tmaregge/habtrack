import { NativeSyntheticEvent, TextInputChangeEventData } from "react-native";

export type HabitType = {
  name: string;
  description: string;
  streak: number;
  pressed: boolean;    // Whether a habit has been pressed or not
  gracePeriod: number; // Number of days before an unchecked habit's streak is reset
  graceRemaining: number;
};

// Aliases
export type TextInputEvent = NativeSyntheticEvent<TextInputChangeEventData>;
export type HabitMap = Map<number, HabitType>;
