import {
  Text,
  View,
  StyleSheet,
  Pressable,
  TextInput,
  Alert,
  Vibration,
} from "react-native";
import * as React from "react";
import { showAndroidToast } from "../utils";
import { globalStyles } from "../styles.js";
import { HabitFunctionsContext } from "../HabitFunctionsContext";
import { TextInputEvent } from "../types";
import { colors } from "../colors";
import { HomeProps } from "./declarations";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";

/**
 * Unified interface for editing/creating habits.
 * Specify mode with the mode prop.
 * Available modes: details, new */
export const HabitEditor = ({ navigation, route }: HomeProps) => {
  const shared = React.useContext(HabitFunctionsContext);

  // mode defaults to new
  let mode = "new";
  let id = 0;

  if (route?.params.mode) mode = route?.params.mode;
  if (route?.params.id) id = route?.params.id;

  // Initialize state variables
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [gracePeriod, setGracePeriod] = React.useState(1);
  const [hasChanged, setHasChanged] = React.useState(false);

  // Thanks to https://reactnavigation.org/docs/preventing-going-back/
  React.useEffect(
    () =>
      navigation.addListener("beforeRemove", (e) => {
        if (!hasChanged) {
          // If we don't have unsaved changes, then we don't need to do anything
          return;
        }

        // Prevent default behavior of leaving the screen
        e.preventDefault();

        // Prompt the user before leaving the screen
        Alert.alert(
          "Discard changes?",
          "You have unsaved changes. Are you sure you want to discard them?",
          [
            { text: "Keep changes", style: "cancel", onPress: () => {} },
            {
              text: "Discard",
              style: "destructive",
              // If the user confirmed, then we dispatch the action we blocked earlier
              // This will continue the action that had triggered the removal of the screen
              onPress: () => navigation.dispatch(e.data.action),
            },
          ]
        );
      }),
    [navigation, hasChanged]
  );

  // Get data from central map if mode is details
  React.useEffect(() => {
    if (mode === "details") {
      try {
        setName(shared.getHabitName(id));
        setDescription(shared.getHabitDescription(id));
        setGracePeriod(shared.getHabitGracePeriod(id));
      } catch (e) {
        console.error(e);
      }
    }
  }, []);

  const onNameChange = (event: TextInputEvent) => {
    setName(event.nativeEvent.text);
    setHasChanged(true);
  };

  const onDescriptionChange = (event: TextInputEvent) => {
    setDescription(event.nativeEvent.text);
    setHasChanged(true);
  };

  // Save a new habit to the central map
  const SaveNewHabitButton = () => {
    const saveHabit = () => {
      const newHabit = {
        streak: 0,
        pressed: false,
        name: name,
        description: description,
        gracePeriod: gracePeriod || 1, // 1 is a fallback value in case something goes wrong
        graceRemaining: gracePeriod || 1, // ditto
      };

      if (newHabit.name != "") {
        shared.saveNewHabit(newHabit);
        navigation.goBack();
      } else {
        showAndroidToast("Please give the habit a name");
      }
    };
    return (
      <Pressable
        style={[globalStyles.button, globalStyles.success]}
        onPress={saveHabit}
        android_ripple={{ color: "aliceblue" }}
      >
        <Text>Create Habit</Text>
      </Pressable>
    );
  };

  // Save changes to an existing habit
  const SaveChangesButton = () => {
    const saveChanges = () => {
      shared.setHabitName(id, name);
      shared.setHabitDescription(id, description);
      shared.setHabitGracePeriod(id, gracePeriod || 1);
      navigation.goBack();
    };
    return (
      <Pressable
        style={[globalStyles.button, globalStyles.success]}
        onPress={saveChanges}
        android_ripple={{ color: "aliceblue" }}
      >
        <Text>Save Changes</Text>
      </Pressable>
    );
  };

  // Input stepper for changing and displaying grace period
  const GracePeriodInput: React.FC = () => {
    const onDecPress = () => {
      if (gracePeriod > 1) {
        setGracePeriod(gracePeriod - 1);
        setHasChanged(true);
      } else {
        Vibration.vibrate(10);
      }
    };

    const onIncPress = () => {
      setGracePeriod(gracePeriod + 1);
      setHasChanged(true);
    };

    return (
      <View style={styles.stepperContainer}>
        <Pressable
          onPress={onDecPress}
          style={styles.stepperButton}
          android_ripple={{ color: "aliceblue" }}
        >
          <Text style={styles.stepperText}>−</Text>
        </Pressable>
        <View style={styles.gpTextContainer}>
          <Text style={styles.gpText}>{gracePeriod.toString()}</Text>
        </View>
        <Pressable
          onPress={onIncPress}
          style={styles.stepperButton}
          android_ripple={{ color: "aliceblue" }}
        >
          <Text style={styles.stepperText}>+</Text>
        </Pressable>
      </View>
    );
  };

  // Menu shown in details mode. Lets you delete a habit
  const DotMenu: React.FC = () => {
    return (
      <View style={styles.dotMenuContainer}>
        <Menu>
          <MenuTrigger>
            <Text style={{ fontSize: 36 }}>...</Text>
          </MenuTrigger>
          <MenuOptions>
            <MenuOption
              onSelect={() => {
                Alert.alert(
                  "Delete",
                  "Really delete this habit?\nThis action cannot be undone",
                  [
                    {
                      text: "Cancel",
                      style: "cancel",
                    },
                    {
                      text: "Confirm Deletion",
                      style: "destructive",
                      onPress: () => {
                        shared.deleteHabit(id);
                        navigation.goBack();
                      },
                    },
                  ]
                );
              }}
            >
              <Text style={{ fontSize: 16, color: "red" }}>Delete</Text>
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  };

  return (
    <View style={styles.editorContainer}>
      <View style={{ flex: 1 }}>
        {mode === "details" && <DotMenu />}
        <View style={styles.habitNameContainer}>
          <TextInput
            style={styles.habitName}
            placeholder={"Habit Name"}
            defaultValue={name}
            onChange={onNameChange}
          ></TextInput>
        </View>
      </View>
      <View style={{ flex: 2 }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.gpLabel}>Grace Period: </Text>
          <GracePeriodInput />
        </View>
        <View style={{ flex: 4, marginTop: 20 }}>
          <Text style={styles.descriptionText}>Description: </Text>
          <TextInput
            multiline
            style={styles.description}
            defaultValue={description}
            onChange={onDescriptionChange}
          ></TextInput>
        </View>
        <View style={{ flex: 1, alignContent: "flex-end" }}>
          {mode === "new" && <SaveNewHabitButton />}
          {mode === "details" && <SaveChangesButton />}
        </View>
      </View>
    </View>
  );
};

export const styles = StyleSheet.create({
  stepperContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  stepperButton: {
    backgroundColor: colors.bgLight,
    borderColor: colors.primaryText,
    borderWidth: 2,
    borderRadius: 100,
    padding: 5,
    margin: 10,
    width: "50%",
    maxWidth: 30,
    maxHeight: 30,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  stepperText: {
    fontSize: 24,
    color: colors.primaryText,
  },
  habitName: {
    fontSize: 36,
    width: "100%",
    textAlign: "left",
  },
  habitNameContainer: {
    flex: 10,
    justifyContent: "center",
  },
  gpTextContainer: {
    flex: 1,
    maxWidth: 30,
    maxHeight: 50,
    margin: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  gpText: {
    fontSize: 24,
    color: colors.primaryText,
  },
  gpLabel: {
    minHeight: 30,
    color: colors.secondaryText,
  },
  dotMenuContainer: {
    flex: 1,
    alignItems: "flex-end",
    marginRight: 20,
  },
  editorContainer: {
    flex: 1,
    marginLeft: 10,
  },
  description: {
    flex: 1,
    width: "95%",
    padding: 5,
    marginTop: 5,
    marginBottom: 10,
    borderWidth: 2,
    borderColor: colors.bgdGray,
    textAlignVertical: "top",
  },
  descriptionText: {
    color: colors.secondaryText,
  },
});
