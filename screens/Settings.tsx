import {
  Text,
  View,
} from "react-native";
import * as React from "react";
import {SettingsProps} from "./declarations"
import { HabitFunctionsContext } from "../HabitFunctionsContext";

export const GlobalSettings = ({ navigation }: SettingsProps) => {
  const shared = React.useContext(HabitFunctionsContext);
  const keys = shared.getHabitsKeys();

  let arr = [];

  for (const key of keys) {
    arr.push(<Text>{shared.getHabitName(key)} : {shared.getHabitGraceRemaining(key)}</Text>);
  }

  return (
    <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
      <Text style={{fontSize: 24}}>There's not much to see here... yet</Text>
      <Text style={{fontSize: 24}}>Grace remaining:</Text>
      {arr}
    </View>
  );
};
