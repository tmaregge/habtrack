import { StyleSheet, Text, View, ScrollView, Pressable } from "react-native";
import * as React from "react";
import Habit from "../components/Habit";
import { Troubleshooting } from "../components/Troubleshooting";
import { HabitFunctionsContext } from "../HabitFunctionsContext";
import { colors } from "../colors";
import { HomeProps } from "./declarations";

export const HabitShowScreen = ({ navigation }: HomeProps) => {
  // View troubleshooting info/tools
  const troubleshooting = false;

  return (
    <>
      <View style={styles.container}>
        <View style={{ flex: 6}}>
    <ScrollView contentContainerStyle={{flexGrow: 1, justifyContent: "center"}}>
            <RenderHabits navigation={navigation} />
          </ScrollView>
        </View>
        <View style={styles.addHabitButtonContainer}>
          <Pressable
            style={styles.addHabitButton}
            onPress={() => {
              navigation.navigate("Editor", { mode: "new" });
            }}
            android_ripple={{ color: "aliceblue" }}
          >
            <Text style={{ fontSize: 24 }}>+</Text>
          </Pressable>
        </View>
        {troubleshooting && <Troubleshooting />}
      </View>
    </>
  );
};

const RenderHabits = ({ navigation }: HomeProps) => {
  const shared = React.useContext(HabitFunctionsContext);

  let arr = [];
  const keys = shared.getHabitsKeys();

  for (const key of keys) {
    arr.push(<Habit id={key} key={key} navigation={navigation} />);
  }

  return <View style={{ flex: 1, justifyContent: "center" }}>{arr}</View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bgLight,
    marginTop: 10,
  },
  addHabitButton: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: colors.success,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 30,
  },
  addHabitButtonContainer: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column-reverse",
    backgroundColor: colors.bgLight,
    elevation: 10,
  },
});
