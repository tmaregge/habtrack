/**
 * Screen declarations
 */
import { View } from "react-native";
import * as React from "react";
import {
  createStackNavigator,
  StackNavigationProp,
} from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { HabitShowScreen } from "./HabitShowScreen";
import { HabitEditor } from "./HabitEditor";
import { GlobalSettings } from "./Settings";
import { RouteProp } from "@react-navigation/native";

type HomeStackParamList = {
  Habits: undefined;
  Editor: { mode: string; id?: number };
};

// Root stack for the left tab
const HomeStack = createStackNavigator<HomeStackParamList>();

export function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Habits" component={HomeScreen} />
      <HomeStack.Screen name="Editor" component={HabitEditor} />
    </HomeStack.Navigator>
  );
}

type HomeScreenNavigationProp = StackNavigationProp<HomeStackParamList, "Habits">;
type HomeScreenRouteProp = RouteProp<HomeStackParamList, "Editor">;

export type HomeProps = {
  navigation: HomeScreenNavigationProp;
  route?: HomeScreenRouteProp;
};

export type HabitProps = {
  navigation: HomeScreenNavigationProp;
  id: number;
}

export function HomeScreen({ navigation }: HomeProps) {
  return (
    <View style={{ flex: 1 }}>
      <HabitShowScreen navigation={navigation} />
    </View>
  );
}

type SettingsStackParamList = {
  Settings: undefined;
};

// Root stack for the right tab
const SettingsStack = createStackNavigator<SettingsStackParamList>();

type SettingsScreenNavigationProp = StackNavigationProp<
  SettingsStackParamList,
  "Settings"
>;

export type SettingsProps = {
  navigation: SettingsScreenNavigationProp;
};

export function SettingsStackScreen() {
  return (
    <SettingsStack.Navigator>
      <SettingsStack.Screen name="Settings" component={SettingsScreen} />
    </SettingsStack.Navigator>
  );
}

export function SettingsScreen({ navigation }: SettingsProps) {
  return <GlobalSettings navigation={navigation} />;
}

export const Tab = createBottomTabNavigator();
