import AsyncStorage from "@react-native-async-storage/async-storage";
import { DateTime } from "luxon";
import { HabitMap } from "./types";
import { printMap } from "./utils";

// AsyncStorage key
const LAST_USED_DATE = "@LastUsedDate";

/** Stores a DateTime obj (@date) as the last used date
 * This date is used to check if it's a new day */
export const storeLastUsedDate = async (date: DateTime) => {
  try {
    await AsyncStorage.setItem(LAST_USED_DATE, date.toISO());
  } catch (e) {
    console.error(e);
  }
};

/** Store a map containing habits (@habitMap) to AsyncStorage.
 * This should only be used with the habits map in App.tsx */
export const storeHabits = async (habitMap: HabitMap) => {
  for (const key of habitMap.keys()) {
    try {
      // AsyncStorage uses string keys, so "@" is prepended to make key a string
      await AsyncStorage.setItem("@" + key, JSON.stringify(habitMap.get(key)));
    } catch (e) {
      console.error("Couldn't store value. Error: " + e);
    }
  }
};

/** Fetches the stored habit map from AsyncStorage. */
export const readHabits = async () => {
  try {
    const keys = await AsyncStorage.getAllKeys();
    let theMap = new Map();
    for (let key of keys) {
      if (key != LAST_USED_DATE) {
        let value = await AsyncStorage.getItem(key);
        if (value) value = JSON.parse(value);
        else throw "Value fetched from AsyncStorage is null";

        key = key.replace("@", ""); // Remove "@" to make key a number again
        theMap.set(key, value);
      }
    }
    return theMap;
  } catch (e) {
    console.error(e);
    return null;
  }
};

/** Deletes a habit with @id from AsyncStorage */
export const removeHabitFromStorage = async (id: number) => {
  const key = "@" + id;

  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    console.log("Could not remove item with key " + key);
  }
};

/** Return DateTime when the app was last used
 *  Returns DateTime.now when it fails, in order not to mess up streaks */
export const getLastUsedDate = async (): Promise<DateTime> => {
  try {
    const lastUsedDate = await AsyncStorage.getItem(LAST_USED_DATE);
    if (lastUsedDate != null) {
      return DateTime.fromISO(lastUsedDate);
    } else {
      return DateTime.now();
    }
  } catch (e) {
    console.error(e);
    return DateTime.now();
  }
};
