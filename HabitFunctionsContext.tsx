import * as React from "react";
import { HabitType } from "./types";

// The actual function definitions are in App.tsx
export interface HabitContextInterface {
  getHabitName: (id: number) => string;
  setHabitName: (id: number, name: string) => void;
  getHabitDescription: (id: number) => string;
  setHabitDescription: (id: number, description: string) => void;
  getHabitStreak: (id: number) => number;
  getHabitsKeys: () => void; // hmm it doesn't actually return void doh
    // It returns some kind of iterator that I wasn't able to annotate (yet)
  isHabitPressed: (id: number) => boolean;
  updateHabitStreak: (id: number) => void;
  saveNewHabit: (newHabit: HabitType) => void;
  deleteHabit: (id: number) => void;
  checkHabitsPressed: () => void;
  getHabitGracePeriod: (id: number) => number;
  setHabitGracePeriod: (id: number, gracePeriod: number) => void;
  getHabitGraceRemaining: (id: number) => number;
}


// I am cheating a bit here, but I think it's OK since this context
// will never be accessed outside of a provider.
export const HabitFunctionsContext =
  React.createContext<HabitContextInterface>({} as HabitContextInterface);

export const HabitFunctionsProvider = HabitFunctionsContext.Provider;
export const HabitFunctionsConsumer = HabitFunctionsContext.Consumer;
