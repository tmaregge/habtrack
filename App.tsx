import { Text } from "react-native";
import * as React from "react";
import { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import Ionicons from "react-native-vector-icons/Ionicons";
import {
  getLastUsedDate,
  readHabits,
  removeHabitFromStorage,
  storeHabits,
  storeLastUsedDate,
} from "./storage";
import { isNewDay, showAndroidToast } from "./utils";
import { DateTime } from "luxon";
import {
  HabitFunctionsProvider,
  HabitContextInterface,
} from "./HabitFunctionsContext";
import {
  HomeStackScreen,
  Tab,
  SettingsStackScreen,
} from "./screens/declarations";
import { HabitType } from "./types";
import { MenuProvider } from "react-native-popup-menu";

/**
 * App: Deals with navigation stuff and habits state.
 *
 * This may be kind of weird, but App basically functions as a sort of API.
 * It stores all the habits in a Map in state and provides functions to
 * change said state.
 *
 * The functions are shared with React Context and is accessed in other
 * components with useContext.
 * */
export default function App() {
  const [habits, setHabits] = useState(new Map());
  const [dataLoaded, setDataLoaded] = useState(false);

  // Reset unpressed habits if true
  const [triggerHabitsCheck, setTriggerHabitsCheck] = React.useState(false);
  // Prevent more than 1 habit reset
  const [habitsTriggered, setHabitsTriggered] = React.useState(false);

  /*=====================*
   * Startup, prep, etc. *
   *=====================*/

  // Load data from storage and trigger a habit check if it's a new day
  useEffect(() => {
    readHabits().then((habitMap) => {
      if (habitMap !== null) {
        setHabits(habitMap);
        getLastUsedDate().then((lastUsedDate) => {
          setDataLoaded(true);
          const now = DateTime.now();
          if (isNewDay(lastUsedDate, now)) {
            setTriggerHabitsCheck(true);
          }
        });
      }
    });
  }, []);

  // Checks if streaks should be checked (should only happen once)
  useEffect(() => {
    if (triggerHabitsCheck && !habitsTriggered) {
      checkHabitsPressed();
      setHabitsTriggered(true);
    }
  }, [triggerHabitsCheck]);

  // Store the date the app was last used, when making changes to habits state.
  // This is used to check if there's been a new day since last time.
  useEffect(() => {
    storeHabits(habits);

    // If dataLoaded is not checked, the lastUsedDate stored by the previous
    // session will be overwritten when the app is launched
    if (dataLoaded) {
      storeLastUsedDate(DateTime.now());
    }
  }, [habits]);

  /*===========*
   * Functions *
   *===========*/

  /** Get the name of the habit with @id */
  const getHabitName = (id: number): string => {
    const name = habits.get(id).name;

    if (!name) throw "Couldn't get name for habit with id: " + id;

    return name;
  };

  /** Set a new @name for the habit with @id */
  const setHabitName = (id: number, name: string): void => {
    if (name != "") {
      let habit = habits.get(id);
      habit.name = name;
      setHabits((prev) => new Map(prev).set(id, habit));
    } else {
      showAndroidToast("Please give the habit a name");
    }
  };

  /** Get the description of the habit with @id */
  const getHabitDescription = (id: number): string => {
    const description = habits.get(id).description;

    if (description === undefined)
      throw "Couldn't get description for habit with id: " + id;

    return description;
  };

  /** Set a new @description for the habit with @id */
  const setHabitDescription = (id: number, description: string): void => {
    let habit = habits.get(id);
    habit.description = description;
    setHabits((prev) => new Map(prev).set(id, habit));
  };

  /** Get the streak of the habit with @id */
  const getHabitStreak = (id: number): number => {
    return habits.get(id).streak;
  };

  /** Check whether habit with @id has been pressed (today) */
  const isHabitPressed = (id: number): boolean => {
    return habits.get(id).pressed;
  };

  /** Increments/decrements the streak of a habit with a given @id */
  const updateHabitStreak = (id: number): void => {
    let habit = habits.get(id);
    if (!habit.pressed) {
      habit.pressed = true;
      habit.streak += 1;
    } else {
      habit.pressed = false;
      habit.streak -= 1;
    }
    setHabits((prev) => new Map(prev).set(id, habit));
  };

  /** Get a new, unused habit id
   * This is only available locally, not as part of the function context (shared) */
  const getNextId = (): number => {
    if (habits.size !== 0) {
      let arr = [];
      const keys = habits.keys();
      for (const key of keys) {
        arr.push(key);
      }
      return Math.max(...arr) + 1;
    } else {
      return 1;
    }
  };

  /** Save a HabitType object to the habits map.
   * @newHabit: habit object to save */
  const saveNewHabit = (newHabit: HabitType) => {
    const id = getNextId();
    setHabits((prev) => new Map(prev).set(id, newHabit));
  };

  /** Delete a habit with @id from the habits map */
  const deleteHabit = (id: number): void => {
    let map = new Map(habits);
    map.delete(id);
    setHabits(map);
    removeHabitFromStorage(id);
  };

  /** Checks all habits to see if they were pressed the previous day.
   * Decrease grace period/reset streak if habit is not pressed, keep streak if habit is pressed */
  const checkHabitsPressed = () => {
    let newmap = new Map(habits);
    for (const key of newmap.keys()) {
      let tmp = newmap.get(key);
      if (tmp.pressed) {
        tmp.pressed = false;
        tmp.graceRemaining = tmp.gracePeriod;
        newmap.set(key, tmp);
      } else {
        tmp.graceRemaining -= 1;
        if (tmp.graceRemaining <= 0) {

          // Update highest streak for habit
          if (!tmp.greatestStreak) {
            tmp.greatestStreak = tmp.streak;
          } else if (tmp.streak > tmp.greatestStreak) {
            tmp.greatestStreak = tmp.streak;
            tmp.greatestStreakDate = DateTime.now();
          }

          // Reset streak
          tmp.streak = 0;
          tmp.graceRemaining = tmp.gracePeriod;
        }
        newmap.set(key, tmp);
      }
    }
    setHabits(newmap);
  };

  /** Get an iterator for the keys of the habits map.
   * Not sure how to annotate this with typescript. */
  const getHabitsKeys = () => {
    return habits.keys();
  };

  /** Returns the @gracePeriod of a habit with @id. */
  const getHabitGracePeriod = (id: number) => {
    const gracePeriod = habits.get(id).gracePeriod;

    if (!gracePeriod) throw "Couldn't get gracePeriod for habit with id: " + id;

    return gracePeriod;
  };

  /** Sets a new @gracePeriod for habit with @id */
  const setHabitGracePeriod = (id: number, gracePeriod: number) => {
    let habit = habits.get(id);
    habit.gracePeriod = gracePeriod;
    setHabits((prev) => new Map(prev).set(id, habit));
  };

  /** Returns @graceRemaining of a habit with @id. */
  const getHabitGraceRemaining = (id: number) => {
    const graceRemaining = habits.get(id).graceRemaining;

    if (!graceRemaining) throw "Couldn't get graceRemaining for habit with id: " + id;

    return graceRemaining;
  };

  // Contains functions that operate on the habits map. This object is shared
  // via context so that child components can read/write data to the map in a
  // controlled manner, without having to pass functions as props.
  const shared: HabitContextInterface = {
    getHabitName,
    setHabitName,
    getHabitDescription,
    setHabitDescription,
    getHabitStreak,
    getHabitsKeys,
    isHabitPressed,
    updateHabitStreak,
    saveNewHabit,
    deleteHabit,
    checkHabitsPressed,
    getHabitGracePeriod,
    setHabitGracePeriod,
    getHabitGraceRemaining,
  };

  if (dataLoaded) {
    return (
      <MenuProvider>
        <HabitFunctionsProvider value={shared}>
          <NavigationContainer>
            <Tab.Navigator
              screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                  let iconName = "";

                  if (route.name === "Habits") {
                    iconName = focused ? "flame" : "flame-outline";
                  } else if (route.name === "Settings") {
                    iconName = focused ? "settings" : "settings-outline";
                  }

                  // You can return any component that you like here!
                  return <Ionicons name={iconName} size={size} color={color} />;
                },
              })}
              tabBarOptions={{
                activeTintColor: "lightblue",
                inactiveTintColor: "gray",
              }}
            >
              <Tab.Screen name="Habits" component={HomeStackScreen} />
              <Tab.Screen name="Settings" component={SettingsStackScreen} />
            </Tab.Navigator>
          </NavigationContainer>
        </HabitFunctionsProvider>
      </MenuProvider>
    );
  } else {
    return <Text>Loading...</Text>;
  }
}
