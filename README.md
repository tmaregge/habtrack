# Habtrack

## About this app
Just a simple habit tracking app I made to learn React Native.

## Screenshots:

![Home page with example habits](screenshots/homepage.png){width=30%}

![Creating a new habit](screenshots/new_habit_page.png){width=30%}

## Build instructions:
Coming soon™
