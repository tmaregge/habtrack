export const DummyData = new Map([
  [
    1,
    {
      streak: 2,
      pressed: false,
      name: "Run 5km",
      description: "Cool description",
      gracePeriod: 2,
      graceRemaining: 2,
    },
  ],
  [
    2,
    {
      streak: 5,
      pressed: false,
      name: "Walk for 30 minutes",
      description: "",
      gracePeriod: 2,
      graceRemaining: 1,
    },
  ],
  [
    3,
    {
      streak: 0,
      pressed: false,
      name: "Brush my teeth",
      description: "Who needs to be reminded of this? CGP Grey?",
      gracePeriod: 1,
      graceRemaining: 1,
    },
  ],
  [
    4,
    {
      streak: 1,
      pressed: false,
      name: "Really long name. Like super long",
      description: "Cool description",
      gracePeriod: 3,
      graceRemaining: 2,
    },
  ],
]);
