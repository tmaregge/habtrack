import AsyncStorage from "@react-native-async-storage/async-storage";
import { DateTime } from "luxon";
import * as React from "react";
import { View, Button } from "react-native";
import { DummyData } from "../dummydata";
import { HabitFunctionsContext } from "../HabitFunctionsContext";
import { getLastUsedDate, readHabits, storeHabits, storeLastUsedDate } from "../storage";
import { printMap } from "../utils";

export const Troubleshooting = () => {
  const LoadHabitsFromStorageButton = () => {
    const replaceMap = () => {
      let storedHabits = tryToLoadFromStorage();
      storedHabits.then((res) => setHabits(new Map(res)));
    };

    return (
      <Button title="Relace habits with stored map" onPress={replaceMap} />
    );
  };

  const CheckHabitsPressedButton = () => {
    const shared = React.useContext(HabitFunctionsContext);

    const handleOnPress = () => {
      shared.checkHabitsPressed();
    };

    return (
      <View>
        <Button title="Check if habits are checked" onPress={handleOnPress} />
      </View>
    );
  };

  const StoreMapButton = () => {
    const handleOnPress = () => {
      storeHabits(habits);
    };

    return (
      <Button
        title="Store the current habit map"
        onPress={() => storeHabits(habits)}
      />
    );
  };

  const ClearStorageButton = () => {
    return <Button title="Clear AsyncStorage" onPress={clearStoredMap} />;
  };

  const clearStoredMap = async () => {
    try {
      await AsyncStorage.clear();
    } catch (e) {
      // clear error
    }

    console.log("Done.");
  };

  const LogAsyncStorageContentButton = () => {
    const handleOnPress = async () => {
      readHabits().then((res) => {
        console.log("Content stored in AsyncStorage:");
        printMap(res);
      });
    };
    return (
      <Button title="Log content in async storage" onPress={handleOnPress} />
    );
  };

  const OverwriteStorageWithDummyDataButton = () => {
    const handleOnPress = async () => {
      try {
        AsyncStorage.clear();
        storeHabits(DummyData);
      } catch (e) {
        console.log(e);
      }
    };

    return (
      <Button
        title="Overwrite AsyncStorage with DummyData"
        onPress={handleOnPress}
      />
    );
  };

  const LogLastUsedDateButton = () => {
    const handleOnPress = () => {
      let date;
      getLastUsedDate().then((res) => {
        console.log("lastUsedDate in AsyncStorage is " + res.toISO());
      });
    }

    return (
      <Button title="Log lastUsedDate" onPress={handleOnPress}/>
    );
  }

  // Simulate new day by setting lastUsedDate to yesterday's date
  const SetLastUsedDateYesterdayButton = () => {
    const handleOnPress = () => {
      const yesterday = DateTime.now().minus({days: 1});
      storeLastUsedDate(yesterday);
    }
    return <Button title="Store yesterday as lastUsedDate" onPress={handleOnPress}/>
  }

  return (
    <>
      <View style={{ flex: 1 }}>
        <CheckHabitsPressedButton />
        <ClearStorageButton />
        <LoadHabitsFromStorageButton />
        <LogAsyncStorageContentButton />
        <OverwriteStorageWithDummyDataButton />
        <LogLastUsedDateButton/>
        <SetLastUsedDateYesterdayButton/>
      </View>
    </>
  );
};
