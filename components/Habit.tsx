import React, { useContext } from "react";
import { Text, StyleSheet, Pressable, View, Vibration } from "react-native";
import { HabitFunctionsContext } from "../HabitFunctionsContext";
import { colors } from "../colors";
import { HabitProps } from "../screens/declarations";

/**
 * Button that shows the name and the streak of a habit.
 * Can be pressed to toggle the streak.
 * Longpress to edit habit details.
 */
function Habit(props: HabitProps) {
  const { navigation } = props;
  const { id } = props;
  const shared = useContext(HabitFunctionsContext);

  const name = shared.getHabitName(id);
  const streak = shared.getHabitStreak(id);
  const isPressed = shared.isHabitPressed(id);
  const gracePeriod = shared.getHabitGracePeriod(id);
  const graceRemaining = shared.getHabitGraceRemaining(id);

  // Warn user that their streak is about to end
  let bgColor = colors.bgLight;
  if (
    !isPressed &&
    streak > 0 &&
    gracePeriod > 1 &&
    graceRemaining < gracePeriod
  ) {
    bgColor = colors.danger;
  } else if (isPressed) {
    bgColor = colors.brandColor;
  }

  const updateStreak = () => {
    Vibration.vibrate(1);
    shared.updateHabitStreak(id);
  };

  return (
    <View>
      <Pressable
        style={[styles.habitButton, { backgroundColor: bgColor }]}
        android_ripple={{ color: "aliceblue" }}
        onPress={updateStreak}
        onLongPress={() =>
          navigation.navigate("Editor", {
            id: id,
            mode: "details",
          })
        }
      >
        <Text style={styles.habitNameText}>{name}</Text>
        <Text style={styles.habitStreakText}>🔥 {streak}</Text>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  habitNameText: {
    textAlign: "left",
    alignSelf: "stretch",
    fontSize: 18,
    flex: 3,
    paddingLeft: 5,
  },
  habitStreakText: {
    fontSize: 18,
    textAlign: "right",
    flex: 1,
    textAlignVertical: "center",
  },
  habitButton: {
    flexDirection: "row",
    alignSelf: "center",
    borderColor: colors.brandColor,
    borderWidth: 4,
    elevation: 2,
    borderRadius: 18,
    marginBottom: 20,
    padding: 15,
    width: "80%",
  },
});

export default Habit;
